ARG VERSION_PHP=7.4

FROM php:${VERSION_PHP}

RUN apt-get update -y && apt-get install -y \
    libzip-dev \
    zip

RUN docker-php-ext-install zip
