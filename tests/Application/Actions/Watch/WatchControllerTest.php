<?php
declare(strict_types=1);

namespace Tests\Application\Actions\Watch;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Application\Handlers\HttpErrorHandler;
use App\Domain\Watch\MySqlWatchRepository;
use DI\Container;
use Nette\Caching\Storage;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;
use Tests\CacheStorageProvider;
use Tests\MySqlRepoProvider;
use Tests\TestCase;

class WatchControllerTest extends TestCase
{

    use MySqlRepoProvider;
    use CacheStorageProvider;


    private App $app;

    protected function setUp(): void
    {
        $this->app = $this->getAppInstance();

        /** @var Container $container */
        $container = $this->app->getContainer();

        $container->set(MySqlWatchRepository::class, $this->provideMysqlRepo());
        $container->set(Storage::class, $this->provideCacheStorage());

        $callableResolver = $this->app->getCallableResolver();
        $responseFactory = $this->app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false ,false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);

        $this->app->add($errorMiddleware);
    }


    public function testFoundAction(): void
    {
        $request = $this->createRequest('GET', '/watch/1');
        $response = $this->app->handle($request);

        $payload = (string) $response->getBody();
        $expectedPayload = new ActionPayload(200, $this->provideDataFromMysql()->toArray());
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }


    public function testNotFoundAction(): void
    {
        $request = $this->createRequest('GET', '/watch/2');
        $response = $this->app->handle($request);

        $payload = (string) $response->getBody();
        $expectedError = new ActionError(ActionError::RESOURCE_NOT_FOUND, 'Watch for 2 not found');
        $expectedPayload = new ActionPayload(404, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

}
