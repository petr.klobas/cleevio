<?php
declare(strict_types=1);

use Nette\Utils\FileSystem;

FileSystem::delete(__DIR__ . '/temp/_watchStore');

require __DIR__ . '/../vendor/autoload.php';
