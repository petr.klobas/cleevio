<?php
declare(strict_types=1);

namespace Tests\Domain\Watch;

use App\Domain\Watch\Exceptions\MySqlWatchNotFoundException;
use App\Domain\Watch\Exceptions\XmlWatchNotFoundException;
use App\Domain\Watch\WatchDataSource;
use App\Domain\Watch\XmlWatchLoader;
use InvalidArgumentException;
use Nette\Caching\Storages\FileStorage;
use PHPUnit\Framework\TestCase;
use stdClass;
use Tests\CacheStorageProvider;
use Tests\MySqlRepoProvider;

class WatchDataSourceTest extends TestCase
{

    use MySqlRepoProvider;
    use CacheStorageProvider;


    public function testMysql(): void
    {
        $dataSource = new WatchDataSource($this->provideMysqlRepo(), $this->provideCacheStorage());

        $this->assertTrue($dataSource instanceof WatchDataSource);

        $this->assertSame($this->provideDataFromMysql()->toArray(), $dataSource->getWatchById(1));

        $this->expectException(MySqlWatchNotFoundException::class);
        $dataSource->getWatchById(2);
    }


    public function testXml(): void
    {
        $dataSource = new WatchDataSource($this->provideXmlLoader(), $this->provideCacheStorage());

        $this->assertTrue($dataSource instanceof WatchDataSource);

        $this->assertSame($this->provideDataFromXml(), $dataSource->getWatchById(1));

        $this->expectException(XmlWatchNotFoundException::class);
        $dataSource->getWatchById(2);
    }


    public function testInvalidInstantiation(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new WatchDataSource(new stdClass(), $this->provideCacheStorage());
    }


    private function provideXmlLoader(): XmlWatchLoader
    {
        return new class($this->provideDataFromXml()) implements XmlWatchLoader {

            private array $data;

            public function __construct(array $array)
            {
                $this->data['1'] = $array;
            }

            public function loadByIdFromXml(string $watchIdentification): ?array
            {
                if (array_key_exists($watchIdentification, $this->data) === false) {
                    throw new XmlWatchNotFoundException();
                }

                return $this->data[$watchIdentification];
            }
        };
    }


    private function provideDataFromXml(): array
    {
        return [
            'identification' => 1,
            'title' => 'first',
            'price' => 20,
            'description' => 'description',
        ];
    }

}
