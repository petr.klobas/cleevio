<?php
declare(strict_types=1);

namespace Tests\Domain\Watch\DTO;

use App\Domain\Watch\DTO\MySqlWatchDTO;
use PHPUnit\Framework\TestCase;

class MySqlWatchDTOTest extends TestCase
{

    /**
     * @dataProvider provideWatchData
     */
    public function testSerialize(array $expected, array $input): void
    {
        $dto = new MySqlWatchDTO(
            $input['identification'],
            $input['title'],
            $input['price'],
            $input['description']
        );

        $this->assertSame($expected, $dto->toArray());
    }


    public function provideWatchData(): array
    {
        $first = [
            'identification' => 1,
            'title' => 'Watch with water fountain',
            'price' => 200,
            'description' => 'Beautifully crafted timepiece for every gentleman.',
        ];

        $second = [
            'identification' => 2,
            'title' => 'Dimension jumper',
            'price' => (int) 1e6,
            'description' => 'Watch with knob that allows you jump between dimensions.',
        ];

        return [
            'first' => [
                $first,
                $first
            ],
            'second' => [
                $second,
                $second
            ],
        ];
    }

}
