<?php declare(strict_types = 1);

namespace Tests;

use Nette\Caching\Storages\FileStorage;

trait CacheStorageProvider
{

    private function provideCacheStorage(): FileStorage
    {
        return new FileStorage(__DIR__ . '/temp/');
    }

}
