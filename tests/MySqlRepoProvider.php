<?php
declare(strict_types=1);

namespace Tests;

use App\Domain\Watch\DTO\MySqlWatchDTO;
use App\Domain\Watch\Exceptions\MySqlWatchNotFoundException;
use App\Domain\Watch\MySqlWatchRepository;

trait MySqlRepoProvider
{

    private function provideMysqlRepo(): MySqlWatchRepository
    {
        return new class($this->provideDataFromMysql()) implements MySqlWatchRepository {

            private array $data;

            public function __construct(MySqlWatchDTO $dto)
            {
                $this->data[1] = $dto;
            }

            public function getWatchById(int $id): MySqlWatchDTO
            {
                if (array_key_exists($id, $this->data) === false) {
                    throw new MySqlWatchNotFoundException();
                }

                return $this->data[$id];
            }
        };
    }


    private function provideDataFromMysql(): MySqlWatchDTO
    {
        return new MySqlWatchDTO(1, 'first', 20, 'description');
    }

}
