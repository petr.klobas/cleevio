<?php
declare(strict_types = 1);

namespace App\Application\Actions\Watch;

use App\Application\Actions\Action;
use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Domain\Watch\Exceptions\MySqlRepositoryException;
use App\Domain\Watch\Exceptions\MySqlWatchNotFoundException;
use App\Domain\Watch\Exceptions\XmlLoaderException;
use App\Domain\Watch\Exceptions\XmlWatchNotFoundException;
use App\Domain\Watch\WatchDataSource;
use Error;
use ErrorException;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Throwable;

class WatchController extends Action
{

    private WatchDataSource $watchDataSource;


    public function __construct(
        LoggerInterface $logger,
        WatchDataSource $watchDataSource
    ) {
        parent::__construct($logger);
        $this->watchDataSource = $watchDataSource;
    }


    /**
     * @param int $id
     *
     * @return array
     * @throws MySqlRepositoryException
     * @throws MySqlWatchNotFoundException
     * @throws XmlWatchNotFoundException
     * @throws XmlLoaderException
     * @throws ErrorException
     * @throws Throwable
     */
    public function getByIdAction(int $id): array
    {
        return $this->watchDataSource->getWatchById($id);
    }


    protected function action(): Response
    {
        $id = $this->resolveArg('id');

        if ($this->validateRequestId($id) === false) {
            return $this->respond(new ActionPayload(
                StatusCodeInterface::STATUS_BAD_REQUEST,
                null,
                new ActionError(ActionError::BAD_REQUEST, 'ID must be positive integer')
            ));
        }

        try {
            $watch = $this->getByIdAction((int) $id);

            return $this->respond(new ActionPayload(StatusCodeInterface::STATUS_OK, $watch));
        } catch (MySqlWatchNotFoundException | XmlWatchNotFoundException $e) {
            return $this->respond(new ActionPayload(
                StatusCodeInterface::STATUS_NOT_FOUND,
                null,
                new ActionError(ActionError::RESOURCE_NOT_FOUND, sprintf('Watch for %s not found', $id))
            ));
        } catch (XmlLoaderException | MySqlRepositoryException | ErrorException | Throwable $e) {
            $this->logger->critical($e->getMessage(), ['request' => $this->request]);

            return $this->respond(new ActionPayload(
                StatusCodeInterface::STATUS_NOT_FOUND,
                null,
                new ActionError(ActionError::SERVER_ERROR, 'Server encountered internal error')
            ));
        }
    }


    private function validateRequestId($id): bool
    {
        if (ctype_digit($id) && $id > 0) {
            return true;
        }

        return false;
    }

}
