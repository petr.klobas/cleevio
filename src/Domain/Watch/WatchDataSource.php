<?php
declare(strict_types=1);

namespace App\Domain\Watch;

use App\Domain\Watch\Exceptions\MySqlRepositoryException;
use App\Domain\Watch\Exceptions\MySqlWatchNotFoundException;
use App\Domain\Watch\Exceptions\XmlLoaderException;
use App\Domain\Watch\Exceptions\XmlWatchNotFoundException;
use ErrorException;
use InvalidArgumentException;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Throwable;

class WatchDataSource
{

    private MySqlWatchRepository $watchRepository;

    private XmlWatchLoader $watchLoader;

    private Cache $cache;


    public function __construct(
        $source,
        Storage $storage
    ) {
        if ($source instanceof MySqlWatchRepository) {
            $this->watchRepository = $source;
        } elseif ($source instanceof XmlWatchLoader) {
            $this->watchLoader = $source;
        } else {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid watch source provided, expected %s or %s, got %s',
                    MySqlWatchRepository::class,
                    XmlWatchLoader::class,
                    gettype($source)
                )
            );
        }

        $this->cache = new Cache($storage, 'watchStore');
    }


    /**
     * @throws ErrorException
     * @throws MySqlRepositoryException
     * @throws MySqlWatchNotFoundException
     * @throws XmlWatchNotFoundException
     * @throws XmlLoaderException
     * @throws Throwable
     */
    public function getWatchById(int $id): array
    {
        return $this->cache->load($id, function () use ($id): array {
            if (isset($this->watchRepository)) {
                $dto = $this->watchRepository->getWatchById($id);

                return $dto->toArray();
            } elseif (isset($this->watchLoader)) {
                $array = $this->watchLoader->loadByIdFromXml((string) $id);

                if ($array === null) {
                    throw new XmlWatchNotFoundException(sprintf('Watch for %s not found in source', $id));
                }

                return $array;
            }

            throw new ErrorException('Watch data source not instantiated.');
        });
    }
}
