<?php
declare(strict_types=1);

namespace App\Domain\Watch\DTO;

class MySqlWatchDTO
{

    public int $id;

    public string $title;

    public int $price;

    public string $description;


    public function __construct(
        int $id,
        string $title,
        int $price,
        string $description
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->description = $description;
    }


    public function toArray(): array
    {
        return [
            'identification' => $this->id,
            'title' => $this->title,
            'price' => $this->price,
            'description' => $this->description,
        ];
    }

}
