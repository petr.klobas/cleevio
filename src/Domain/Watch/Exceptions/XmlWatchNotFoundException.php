<?php
declare(strict_types=1);

namespace App\Domain\Watch\Exceptions;

class XmlWatchNotFoundException extends XmlLoaderException
{

}
