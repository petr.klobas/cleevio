# Cleevio PHP assignment

## About

1. The solution is not runnable as it is (almost) required in assignment by constraint, that interfaces `MySqlWatchRepository` and `XmlWatchLoader` should not be implemented in solution.  
2. Aforementioned interfaces are implemented, but **ONLY** in tests to verify that the solution works.
3. To run tests, install dependencies with composer and then run `composer test`.
4. You can utilize prepared docker environment as per your customs.

## Comments
1. Solution utilizes Slim framework with PHP-DI library to inject dependencies.
2. Configurable datasource (as required by assignemnt) is available in `app/dependencies.php` annotated in comments, datasource is interchangable with effect to the domain code itself.
