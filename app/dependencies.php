<?php
declare(strict_types=1);

use App\Application\Settings\SettingsInterface;
use App\Domain\Watch\MySqlWatchRepository;
use App\Domain\Watch\WatchDataSource;
use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Nette\Caching\Storage;
use Nette\Caching\Storages\FileStorage;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c): LoggerInterface {
            $settings = $c->get(SettingsInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
    ]);

//    $containerBuilder->addDefinitions([
//        XmlWatchLoader::class => \DI\autowire(XmlWatchLoader::class),
//    ]);

    $containerBuilder->addDefinitions([
        MySqlWatchRepository::class => \DI\autowire(MySqlWatchRepository::class),
    ]);

    $containerBuilder->addDefinitions([
        WatchDataSource::class => function (ContainerInterface $container): WatchDataSource {
            return new WatchDataSource(
            // $container->get(XmlWatchLoader::class)
                $container->get(MySqlWatchRepository::class),
                $container->get(Storage::class)
            );
        },
    ]);

    $containerBuilder->addDefinitions([
        Storage::class => function (): Storage {
            return new FileStorage(__DIR__ . '/../var/cache');
        }
    ]);

};
